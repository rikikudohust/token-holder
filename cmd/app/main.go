package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
  "gitlab.com/rikikudohust/token-holder/internal/pkg/builder"
)

func main() {
	app := &cli.App{
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "config",
				Aliases: []string{"c"},
				Value:   "internal/pkg/config/job_fantom.yaml",
				Usage:   "Configuration file",
			},
		},
		Commands: []*cli.Command{
			{
				Name:  "scan",
				Usage: "Run scanner",
				Action: func(c *cli.Context) (err error) {
					s, err := builder.NewScanner(c.String("config"))
					if err != nil {
						return err
					}
					return s.Run()
				},
			},
			{
				Name:  "collect",
				Usage: "Run collector",
				Action: func(c *cli.Context) (err error) {
					collector, err := builder.NewCollector()
					if err != nil {
						return err
					}
					return collector.Run()
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

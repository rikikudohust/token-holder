package builder

import (
	"sync"

	"gitlab.com/rikikudohust/token-holder/internal/pkg/model"
	"gitlab.com/rikikudohust/token-holder/internal/pkg/service/holder"

	"gitlab.com/firebird-finance/backend/go-common/pkg/context"
)

type Collector struct {
	jobs map[string]model.IJob
}

func NewCollector() (*Collector, error) {
	dbCfg, err := LoadEnvConfig()
	if err != nil {
		return nil, err
	}

	db, err := NewPostgres(dbCfg)
	if err != nil {
		return nil, err
	}

	jobs := map[string]model.IJob{
		model.ServiceTokenHolder: holder.NewJob(db),
	}

	return &Collector{
		jobs: jobs,
	}, nil
}

func (c *Collector) Run() error {
	var wg sync.WaitGroup
	wg.Add(len(c.jobs))

	for name, job := range c.jobs {
		ctx := context.NewDefault().WithRequestID(false)
		ctx = ctx.WithLogPrefix(name)
		go func(c context.Context, j model.IJob) {
			defer wg.Done()
			j.Run(c)
		}(ctx, job)
	}

	wg.Wait()
	return nil
}

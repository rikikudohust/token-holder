package builder

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
	"github.com/mcuadros/go-defaults"
	"github.com/spf13/viper"
  "token-holder/internal/pkg/model"
)

type jobConfig struct {
	Config   model.JobConfig
	Database dbConfig
}
func loadJobConfig(file string) (*jobConfig, error) {
	viper.SetConfigFile(file)
	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}
	c := &jobConfig{}
	defaults.SetDefaults(c)
	if err := viper.Unmarshal(c); err != nil {
		return nil, err
	}

	dbConfig, err := LoadEnvConfig()
	if err != nil {
		return nil, err
	}
	c.Database = *dbConfig

	fmt.Printf("config: %+v\n", c.Config)
	return c, nil
}

func LoadEnvConfig() (*dbConfig, error) {
	var dbCfg dbConfig
	if err := envconfig.Process("DB", &dbCfg); err != nil {
		return nil, err
	}


	return &dbCfg, nil
}

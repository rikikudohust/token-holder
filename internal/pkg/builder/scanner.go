package builder

import (
	"sync"

	"gitlab.com/rikikudohust/token-holder/internal/pkg/model"
	"gitlab.com/rikikudohust/token-holder/internal/pkg/service/event"
	"gitlab.com/rikikudohust/token-holder/internal/pkg/service/balance"

	"gitlab.com/firebird-finance/backend/go-common/pkg/context"
)

type Scanner struct {
	cfg  model.JobConfig
	jobs map[string]model.IJob
}

func NewScanner(configFile string) (*Scanner, error) {
	c, err := loadJobConfig(configFile)
	if err != nil {
		return nil, err
	}

	db, err := NewPostgres(&c.Database)
	if err != nil {
		return nil, err
	}


	jobs := map[string]model.IJob{
    model.ServiceTransferEvent: event.NewJob(&c.Config, db),
    model.ServiceBalance: balance.NewJob(&c.Config, db),
	}

	return &Scanner{
		cfg:  c.Config,
		jobs: jobs,
	}, nil
}

func (s *Scanner) Run() error {
	var wg sync.WaitGroup
	wg.Add(len(s.jobs))

	for _, name := range s.cfg.Jobs {
		ctx := context.NewDefault().WithRequestID(false)
		ctx = ctx.WithLogPrefix(name)
		job, existed := s.jobs[name]
		if !existed {
			ctx.Warnf("service not supported")
			continue
		}
		go func(c context.Context, j model.IJob) {
			defer wg.Done()
			j.Run(c)
		}(ctx, job)
	}

	wg.Wait()
	return nil
}

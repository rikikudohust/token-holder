package model

import (
	"github.com/ethereum/go-ethereum/common"
)

type LogTransfer struct {
	From   common.Address
	To     common.Address
	Value  *BigInt
	Block  uint64
}

type LogApproval struct {
	TokenOnwer common.Address
	Spender    common.Address
	Amount    BigInt 
}

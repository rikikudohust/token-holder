package model

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"

	"gitlab.com/firebird-finance/backend/go-common/pkg/context"
)

const (
	EnvProd = "prod"
	EnvDev  = "dev"
)

const (
	ServiceTokenHolder   = "tokenholder"
	ServiceTransferEvent = "event"
	ServiceBalance       = "balance"
)
const (
	GormDataTypeJSON = "json"
	GormDataTypeText = "text"
)

const (
	ChainIDMumbai    = 80001
	ChainIDAvalanche = 43114
	ChainIDArbitrum  = 42161
	ChainIDCanto     = 7700
	ChainIDDogechain = 2000
	ChainIDMoonriver = 1285
	ChainIDZKSync    = 324
	ChainIDFantom    = 250
	ChainIDPolygon   = 137
	ChainIDBsc       = 56
	ChainIDCronos    = 25
	ChainIDOptimism  = 10
	ChainIDEthereum  = 1
)

var ChainNames = map[uint64]string{
	ChainIDMumbai:    "mumbai",
	ChainIDAvalanche: "avalanche",
	ChainIDArbitrum:  "arbitrum",
	ChainIDDogechain: "dogechain",
	ChainIDMoonriver: "moonriver",
	ChainIDFantom:    "fantom",
	ChainIDPolygon:   "polygon",
	ChainIDBsc:       "bsc",
	ChainIDCronos:    "cronos",
	ChainIDOptimism:  "optimism",
	ChainIDEthereum:  "ethereum",
}

const (
	DecBase = 10
	HexBase = 16
)

const (
	SecPerHour = 3600
	SecPerDay  = SecPerHour * 24
	SecPerWeek = SecPerDay * 7
)

type IJob interface {
	Run(ctx context.Context)
}

type Map map[string]interface{}

func (m *Map) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("failed to unmarshal JSONB value: %v", value)
	}
	return json.Unmarshal(bytes, m)
}

func (m Map) Value() (driver.Value, error) {
	return json.Marshal(m)
}

func (Map) GormDataType() string {
	return GormDataTypeJSON
}

type StringArray []string

func (sa *StringArray) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("failed to unmarshal JSONB value: %v", value)
	}
	return json.Unmarshal(bytes, sa)
}

func (sa StringArray) Value() (driver.Value, error) {
	return json.Marshal(sa)
}

func (StringArray) GormDataType() string {
	return GormDataTypeJSON
}

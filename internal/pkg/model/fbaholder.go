package model

import (
	"github.com/ethereum/go-ethereum/common"
)

type User struct {
	Address common.Address
}

type Balance struct {
	Token   common.Address
	Address common.Address
	Balance *BigInt
	Block   uint64
}

package model

type JobConfig struct {
	Network         string
	ChainID         uint64
	ConfirmedBlocks uint64
	BlockPerDay     uint64
	NativeToken     string
	WrapToken       string
	Multicall       string
	Jobs            []string
	RPCs            []string
	APIs            JobConfigAPIs
}

type JobConfigAPIs struct {
	RouterAPI string
}

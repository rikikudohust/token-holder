package model

import (
	"database/sql/driver"
	"fmt"
	"math/big"
)

type BigInt big.Int

func NewBigInt(d int64) *BigInt {
	return (*BigInt)(big.NewInt(d))
}

func NewBigIntFromString(s string) *BigInt {
	b, ok := new(big.Int).SetString(s, 0)
	if !ok {
		b = big.NewInt(0)
	}
	return (*BigInt)(b)
}

func NewBigIntFromFloat(f float64) *BigInt {
	b, _ := big.NewFloat(f).Int(nil)
	return (*BigInt)(b)
}

func (b *BigInt) BI() *big.Int {
	return (*big.Int)(b)
}

func (b *BigInt) Cmp(y *BigInt) int {
	return (*big.Int)(b).Cmp((*big.Int)(y))
}

func (b *BigInt) Add(y *BigInt) *BigInt {
	if y == nil || y.Cmp(NewBigInt(0)) == 0 {
		return b
	}
	if b == nil {
		r := *y
		return &r
	}
	return (*BigInt)((*big.Int)(b).Add((*big.Int)(b), (*big.Int)(y)))
}

func (b *BigInt) Sub(y *BigInt) *BigInt {
	if y == nil || y.Cmp(NewBigInt(0)) == 0 {
		return b
	}
	if b == nil {
		r := *y
		return &r
	}
	return (*BigInt)((*big.Int)(b).Sub((*big.Int)(b), (*big.Int)(y)))
}

func (b *BigInt) Mul(y *BigInt) *BigInt {
	if y == nil || y.Cmp(NewBigInt(1)) == 0 {
		return b
	}
	if b == nil {
		r := *y
		return &r
	}
	return (*BigInt)((*big.Int)(b).Mul((*big.Int)(b), (*big.Int)(y)))
}

func (b *BigInt) Div(y *BigInt) *BigInt {
	if y == nil || y.Cmp(NewBigInt(1)) == 0 {
		return b
	}
	if b == nil {
		r := *y
		return &r
	}
	return (*BigInt)((*big.Int)(b).Div((*big.Int)(b), (*big.Int)(y)))
}

func (b *BigInt) F64() float64 {
	res, _ := new(big.Float).SetInt((*big.Int)(b)).Float64()
	return res
}

func (b *BigInt) String() string {
	return (*big.Int)(b).String()
}

func (b *BigInt) Hex() string {
	h := (*big.Int)(b).Text(HexBase)
	if len(h)%2 == 0 {
		return "0x" + h
	}
	return "0x0" + h
}

func (b *BigInt) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%q", b.String())), nil
}

func (b *BigInt) Scan(value interface{}) error {
	stringValue, ok := value.(string)
	if !ok {
		return fmt.Errorf("failed to get string value: %v", value)
	}
	(*big.Int)(b).SetString(stringValue, DecBase)
	return nil
}

func (b BigInt) Value() (driver.Value, error) {
	return (*big.Int)(&b).String(), nil
}

func (BigInt) GormDataType() string {
	return GormDataTypeText
}

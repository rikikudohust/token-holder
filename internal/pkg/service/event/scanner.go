package event

import (
	"math/big"
	"time"

	"gitlab.com/rikikudohust/token-holder/internal/pkg/model"
	"gitlab.com/rikikudohust/token-holder/internal/pkg/utils"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/firebird-finance/backend/go-common/pkg/context"
	"gorm.io/gorm"
)

type job struct {
	globalConfig *model.JobConfig
	localConfig  *config
	db           *gorm.DB
	abi          abi.ABI
}

func NewJob(cfg *model.JobConfig, db *gorm.DB) model.IJob {
	return &job{
		globalConfig: cfg,
		db:           db,
		abi:          utils.GetABI("internal/pkg/abis/FBA.json"),
	}
}

func (j *job) Run(ctx context.Context) {
	c, existed := configs[j.globalConfig.ChainID]
	if !existed {
		ctx.Warnln("not support network", j.globalConfig.ChainID)
	}
	j.localConfig = &c
	var currentBlock uint64
	for {
		start := time.Now()
		ctx.Infoln("start")
		j.process(ctx, &currentBlock)
		ctx.Infoln("done, elapsed:", time.Since(start))
		time.Sleep(time.Duration(j.localConfig.JobIntervalSecond) * time.Second)
	}

}

func (j *job) process(ctx context.Context, currentBlock *uint64) {
	syncingBlock := *currentBlock

	transfers, syncedBlock, err := j.getLogs(ctx, syncingBlock)
	if err != nil {
		ctx.Errorf("failed to get logs, err: %v", err)
		return
	}
	tx := j.db.CreateInBatches(transfers, len(transfers))
	if tx.Error != nil {
		ctx.Errorf("failed to create new transfers data, err: %v", tx.Error)
	}

	ctx.Infof("saving %d transfer event to db", len(transfers))
	*currentBlock = syncedBlock
}

func (j *job) getLogs(ctx context.Context, syncingBlock uint64) (transfers []*model.LogTransfer, syncedBlock uint64, err error) {
	if j.localConfig.StartBlock > syncingBlock {
		syncingBlock = j.localConfig.StartBlock
	}

	client, err := utils.GetEvmClient(ctx, j.globalConfig.RPCs)
	if err != nil {
		ctx.Errorf("fail to get evm client, err: %v", err)
		return nil, 0, err
	}

	eventQuery := ethereum.FilterQuery{
		Topics:    [][]common.Hash{{j.localConfig.TransferTopic}},
		Addresses: j.localConfig.Token,
	}

	var toBlock uint64
	skip := uint64(0)

	for {
		eventQuery.FromBlock = big.NewInt(int64(syncingBlock + skip))
		toBlock = syncingBlock + j.localConfig.BlockInterval - 1 + skip
		eventQuery.ToBlock = big.NewInt(int64(toBlock))

		ctx.Infof("quering from block %v to %v", eventQuery.FromBlock, eventQuery.ToBlock)
		logs, err := client.FilterLogs(ctx, eventQuery)
		if err != nil {
			ctx.Errorf("failed to get network logs, err: %v", err)
		}

		ctx.Infoln("len(logs): ", len(logs))

		for _, log := range logs {
			var logTransfer struct {
				Value *big.Int
			}

			if err := j.abi.UnpackIntoInterface(&logTransfer, "Transfer", log.Data); err != nil {
				ctx.Errorf("failed to unpack log data, err %v", err)
			}
			transfers = append(transfers, &model.LogTransfer{
				From:  common.BytesToAddress(log.Topics[1].Bytes()),
				To:    common.BytesToAddress(log.Topics[2].Bytes()),
				Value: (*model.BigInt)(logTransfer.Value),
				Block: log.BlockNumber,
			})

			if len(transfers) >= int(j.localConfig.BatchSize) {
				return transfers, transfers[len(transfers)-1].Block, nil
			}
		}

		if len(transfers) < int(j.localConfig.BatchSize) {
			skip = j.localConfig.BlockInterval
			syncingBlock = eventQuery.FromBlock.Uint64()
		}
	}
}

package balance

import (
	"time"
	"gitlab.com/rikikudohust/token-holder/internal/pkg/model"
	"gitlab.com/rikikudohust/token-holder/internal/pkg/utils"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/firebird-finance/backend/go-common/pkg/context"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type job struct {
	globalConfig *model.JobConfig
	localConfig  *config
	db           *gorm.DB
	abi          abi.ABI
}

func NewJob(cfg *model.JobConfig, db *gorm.DB) model.IJob {
	return &job{
		globalConfig: cfg,
		db:           db,
		abi:          utils.GetABI("internal/pkg/abis/FBA.json"),
	}
}

func (j *job) Run(ctx context.Context) {
	c, existed := configs[j.globalConfig.ChainID]
	if !existed {
		ctx.Warnln("not support network", j.globalConfig.ChainID)
	}
	j.localConfig = &c
	for {
		start := time.Now()
		ctx.Infoln("start")
		j.process(ctx, j.localConfig.EndBlock)
		ctx.Infoln("done, elapsed:", time.Since(start))
		time.Sleep(time.Duration(j.localConfig.JobIntervalSecond) * time.Second)
	}
}

func (j *job) process(ctx context.Context, blockNumber uint64) {
	start := time.Now()

	ctx.Infoln("start")
	query := j.db.Table("log_transfers")
	query = query.Select("*")
	query = query.Where("block <= ?", blockNumber)

	var logTransfers []*model.LogTransfer
	if err := query.Scan(&logTransfers).Error; err != nil {
		ctx.Errorf("failed to get transfer event from db, err: %v", err)
		return
	}

	userData := make(map[common.Address]*model.BigInt)
	for _, l := range logTransfers {
		if userData[l.From] == nil {
			userData[l.From] = model.NewBigInt(0)
		}
		if userData[l.To] == model.NewBigInt(0) {
			userData[l.To] = model.NewBigInt(0)
		}
		if l.From != common.HexToAddress("0x0000000000000000000000000000000000000000") {
			userData[l.From] = userData[l.From].Sub(l.Value)
		}
		if l.To != common.HexToAddress("0x0000000000000000000000000000000000000000") {
			userData[l.To] = userData[l.To].Add(l.Value)
		}
	}

	balances := make([]*model.Balance, 0, len(logTransfers))
	for user, balance := range userData {
		balances = append(balances, &model.Balance{
			Token:   common.HexToAddress("0x0e249130b3545a2a287de9f27d805cab95f03db9"),
			Address: user,
			Balance: balance,
			Block:   blockNumber,
		})
	}

	ctx.Infof("saving %d users to db", len(balances))
	if len(balances) > 0 {
		if err := j.db.Clauses(clause.OnConflict{
			DoNothing: true,
		}).CreateInBatches(balances, len(balances)).Error; err != nil {
			ctx.Errorf("failed to insert users to database, err: %v", err)
			return
		}

		ctx.Infoln("done, elapsed:", time.Since(start))
	}

}

// func (j *job) getERC20BalanceAtBlock(ctx context.Context, blockNumber uint64, token common.Address) (balance []*model.Balance, err error) {
// 	cli, err := utils.GetEvmClient(ctx, j.globalConfig.RPCs)
// 	if err != nil {
// 		ctx.Errorf("failed to connect client, err %v", err)
// 	}
// 	query := j.db.Table("users")
// 	query = query.Select("*")
// 	query = query.Limit(100)
//
// 	var users []*model.User
// 	if err := query.Scan(&users).Error; err != nil {
// 		ctx.Errorf("failded to get users from db, err: %v", err)
// 	}
//
// 	data := make([]byte, 0)
// 	for _, user := range users {
// 		balanceOfFunc := j.abi.Methods["balanceOf"]
// 		input, err := balanceOfFunc.Inputs.Pack(user.Address)
// 		if err != nil {
// 			return nil, err
// 		}
// 		data = append(data, input...)
// 	}
//
// 	msg := ethereum.CallMsg{
// 		To:   &token,
// 		Data: data,
// 	}
//
// 	output, err := cli.CallContract(ctx, msg, big.NewInt(int64(blockNumber)))
//
// 	if err != nil {
// 		return nil, err
// 	}
// 	fmt.Println(output)
//
// 	// balances := make([]*model.Balance, 0, len(users))
// 	// for i := range balances {
// 	//   balanceOfFunc := j.abi.Methods["balanceOf"]
// 	//   outputSize := balanceOfFunc.Outputs
// 	//   result := model.NewBigInt()
// 	//   result.SetB
// 	// }
// 	return nil, nil
// }

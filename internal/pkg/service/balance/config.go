package balance

import (
	"github.com/ethereum/go-ethereum/common"
)

const (
	ChainIDFantom = 250
)

type config struct {
	Token             []common.Address
	JobIntervalSecond uint64
	StartBlock        uint64
	EndBlock          uint64
	BlockInterval     uint64
	BatchSize         uint64
}

var configs = map[uint64]config{
	ChainIDFantom: {
		Token:             []common.Address{common.HexToAddress("0x0e249130b3545a2a287DE9f27d805CAB95f03DB9")},
		JobIntervalSecond: 3,
		StartBlock:        60637788,
		EndBlock:          60637788,
		BlockInterval:     3000,
		BatchSize:         1000,
	},
}


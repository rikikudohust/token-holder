package holder

import (
	"fmt"
	"time"
	"token-holder/internal/pkg/model"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/firebird-finance/backend/go-common/pkg/context"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	jobInterval = 10
	timeOffset  = 3600
)

type job struct {
	db *gorm.DB
}

func NewJob(db *gorm.DB) model.IJob {
	return &job{db: db}
}

func (j *job) Run(ctx context.Context) {
	for {
		go j.process(ctx)
		time.Sleep(time.Duration(jobInterval) * time.Second)
	}
}

func (j *job) process(ctx context.Context) {
	start := time.Now()

	ctx.Infoln("start")
	query := j.db.Table("log_transfers")
	query = query.Select("*")

	var logTransfers []*model.LogTransfer
	if err := query.Scan(&logTransfers).Error; err != nil {
		ctx.Errorf("failed to get transfer event from db, err: %v", err)
		return
	}

	userData := make(map[common.Address]common.Address)
	for _, l := range logTransfers {
    fmt.Println(l.From)
    fmt.Println(l.To)
		if l.From != common.HexToAddress("0x0000000000000000000000000000000000000000") {
			userData[l.From] = l.From

		}
		if l.To != common.HexToAddress("0x0000000000000000000000000000000000000000") {
			userData[l.To] = l.To
		}
	}

	users := make([]*model.User, 0, len(logTransfers))
	for user := range userData {
		users = append(users, &model.User{
			Address: user,
		})
	}

	ctx.Infof("saving %d users to db", len(users))
	if len(users) > 0 {
		if err := j.db.Clauses(clause.OnConflict{
			DoNothing: true,
		}).CreateInBatches(users, len(users)).Error; err != nil {
			ctx.Errorf("failed to insert users to database, err: %v", err)
			return
		}

		ctx.Infoln("done, elapsed:", time.Since(start))
	}

}

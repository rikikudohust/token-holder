package holder

import (
	"token-holder/internal/pkg/model"

	"github.com/ethereum/go-ethereum/common"
)

const (
	ChainIDFantom = 250
)

type config struct {
	Token          []common.Address
	JobIntervalSec uint64
	BatchSize      uint64
}

var configs = map[uint64]config{
	model.ChainIDFantom: {
		Token:          []common.Address{common.HexToAddress("0x0e249130b3545a2a287DE9f27d805CAB95f03DB9")},
		JobIntervalSec: 6,
    BatchSize: 50,
	},
}

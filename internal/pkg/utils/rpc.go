package utils

import (
	"crypto/rand"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/firebird-finance/backend/go-common/pkg/context"
)

func GetEvmClient(ctx context.Context, rpcs []string) (*ethclient.Client, error) {
	rpcsLen := len(rpcs)
	indexRand, err := rand.Int(rand.Reader, big.NewInt(int64(rpcsLen)))
	if err != nil {
		return nil, err
	}
	index := int(indexRand.Int64())

	for i := 0; i < rpcsLen; i++ {
		rpc := rpcs[(index+i)%rpcsLen]
		client, err := ethclient.Dial(rpc)
		if err == nil {
			return client, nil
		}
		ctx.Warnf("failed to connect %s, err: %v", rpc, err)
	}

	return nil, fmt.Errorf("failed to connect any rpcs")
}

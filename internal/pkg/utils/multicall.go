package utils

import (
	"math/big"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/firebird-finance/backend/go-common/pkg/context"
)

type MulticallRequest struct {
	ABI    abi.ABI
	Target common.Address
	Method string
	Params []interface{}
	Output interface{}
}

type multicallParam struct {
	Target   common.Address
	CallData []byte
}

func ProcessMulticall(ctx context.Context, address string, rpcs []string, requests []*MulticallRequest) error {
	var params []multicallParam
	for _, r := range requests {
		d, err := r.ABI.Pack(r.Method, r.Params...)
		if err != nil {
			ctx.Errorf("failed to build call data for %s, err: %v", r.Method, err)
			return err
		}
		params = append(params, multicallParam{Target: r.Target, CallData: d})
	}

	multicallABI := GetABI("internal/pkg/abis/Multicall.json")
	data, err := multicallABI.Pack("aggregate", params)
	if err != nil {
		ctx.Errorf("failed to build multi call data, err: %v", err)
		return err
	}

	ethCli, err := GetEvmClient(ctx, rpcs)
	if err != nil {
		ctx.Errorf("failed to get eth client, err: %v", err)
		return err
	}

	multiCallAddress := common.HexToAddress(address)
	msg := ethereum.CallMsg{To: &multiCallAddress, Data: data}
	resp, err := ethCli.CallContract(ctx, msg, nil)
	if err != nil {
		ctx.Errorf("failed to call multicall, err: %v", err)
		return err
	}

	var result struct {
		BlockNumber *big.Int
		ReturnData  [][]byte
	}
	err = multicallABI.UnpackIntoInterface(&result, "aggregate", resp)
	if err != nil || len(result.ReturnData) != len(requests) {
		ctx.Errorf("failed to unpack multicall response, err: %v", err)
		return err
	}

	for i, r := range requests {
		if _, ok := r.Output.(map[string]interface{}); ok {
			err = r.ABI.UnpackIntoMap(r.Output.(map[string]interface{}), r.Method, result.ReturnData[i])
			if err != nil {
				ctx.Errorf("failed to unpack into map call %s, err: %v", r.Method, err)
				return err
			}
		} else {
			if err = r.ABI.UnpackIntoInterface(r.Output, r.Method, result.ReturnData[i]); err != nil {
				ctx.Errorf("failed to unpack call %s, err: %v", r.Method, err)
				return err
			}
		}
	}

	return nil
}
